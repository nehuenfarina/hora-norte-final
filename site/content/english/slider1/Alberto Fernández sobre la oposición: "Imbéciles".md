+++
author = ""
date = 2021-04-08T03:00:00Z
description = ""
image = "/images/f608x342-167582_197305_0.jpg"
image_webp = "/images/f608x342-167582_197305_0.jpg"
ttile = "ALBERTO FERNÁNDEZ CALIFICÓ DE \"IMBECILES\" Y \"MISERABLES\" A LA OPOSICIÓN"

+++

**_El presidente Alberto Fernández cuestionó a los dirigentes que criticaron las nuevas medidas restrictivas frente al avance de la segunda ola de coronavirus y calificó de “imbéciles” y “miserables“ a quienes creen que el decreto que se conocerá mañana responde a intereses políticos y no a una necesidad sanitaria._**

“Ayer leía un imbécil que me llamaba dictador, ¿Cuál es la dictadura que estoy ejerciendo? ¿cuidar a la gente, decirles que tengan cuidado? ¿alguien piensa que el que gobierna un país gana haciendo política con la cantidad de contagiados? Hay que ser un imbécil profundo o una muy mala persona”, introdujo en diálogo con Reynaldo Sietecase en Radio Con Vos.

Consultado por los blancos de su crítica, el Presidente aseguró que “son un montón de dirigentes”, sin brindar nombres propios, que lo dicen públicamente. “Dicen que estoy haciendo esto para evitar las PASO ¡Pero, por favor! ¿Cómo alguien puede pensar semejante barrabasada?”, señaló.

Y agregó: “Cuando la historia se escriba yo quiero que me pongan del lado de la gente que cuida la gente de los argentinos. Si pierdo una elección, la pierdo. Pero duermo tranquilo”.

Algunos instantes después, retomó el tema: “Yo quiero descansar en paz antes de morir. Sé que estoy haciendo todo lo que me deja en paz con mi conciencia. Después que digan lo que quieran, que hablen con la brutalidad que hablan, que mientan como mienten... no me importa. Me importa que los argentinos dejen de contagiarse, que no se muera más gente en una pandemia que está afectando al mundo entero”.

“¿Ustedes piensan que yo quiero estar en un país donde la economía caiga, la pobreza crezca y el trabajo falte? Realmente hay que ser muy miserable para inducir a la gente a creer esas cosas”, remarcó.

Luego, Alberto Fernández se refirió al mensaje del jefe de gobierno porteño, Horacio Rodríguez Larreta, que en su conferencia de prensa de este jueves reconoció que no está de acuerdo con la restricción a la circulación nocturna impuesta por el gobierno nacional para combatir la segunda ola de coronavirus, aunque aclaró que acatarán lo que disponga el decreto que se conocerá en las próximas horas.

“No siento que hayamos tenido diferencias, creo que nos pusimos de acuerdo. No sé si le gusta más o menos pero lo que sí estamos viendo, y cuando hablo conmigo lo compartió, es que la circulación nocturna, como convoca al vínculo social, tiene un efecto muy perjudicial a la hora de los contagios”, expresó. “Por eso se ha cerrado la nocturnidad en todo el mundo, no es que se me ocurrió a mí”.

Y destacó: “Creo que nos entendimos: después de las 12 de la noche no puede haber gente paveando por las calle o en reuniones callejeras”.

“La circulación de la que nosotros hablamos el la misma que habla Larreta”, indicó e ilustró cómo será el funcionamiento de la medida con dos ejemplos: “Una persona que saca a pasear el perro a las 12 no es un problema, pero cuatro chicos que se juntan en una plaza a la noche, eso no debe ser. Creo que todos entendemos que no se puede andar en la calle con otra gente exponiéndose al riesgo de contagio”.

También respaldó la definición del gobernador bonaerense, Axel Kicillof, que esta mañana remarcó que “la situación cambió violenta y súbitamente” con el aumento de los contagios y advirtió: “No es una ola, es un tsunami”.

“Objetivamente hay un aumento de casos que francamente es preocupante. Nadie puede decir que es producto de que se testee más, el tema es que ahora de los testeados hay un porcentaje muy alto que da positivo”, indicó y se refirió nuevamente a los desentendimientos con el jefe de gobierno porteño.

“Larreta me dijo ‘esperemos porque tal vez es un rebrote como el de enero y lo controlamos’, pero pasó el tiempo y se extendió el problema y entendió que era necesario tomar estas decisiones. Me llamaría la atención que difiera mucho de lo que estoy diciendo porque lo hablamos claramente”, aseguró y admitió que tanto Rodríguez Larreta como Axel Kicillof tenían reparos respecto del cierre de bares y restaurantes. “Atendí el planteo de Horacio; Axel tampoco estaba de acuerdo en cerrar a las 22. Pero como no soy un necio y un testarudo, y me explicaron que no había mucha diferencia en el impacto, accedí a eso para que todos los cumplamos”.