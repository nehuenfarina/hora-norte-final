+++
author = ""
date = 2020-09-14T03:00:00Z
description = "El cronograma del IFE continuará mañana nuevamente con los documentos cuyo último número es 7."
image = "/images/0037803095-1.jpg"
image_webp = "/images/0037803095.jpg"
title = "Hoy cobran el IFE los que informaron CBU y sus DNI terminan en 7"

+++
El Ingreso Familiar de Emergencia (IFE) será abonado hoy a los beneficiarios y beneficiarias con terminación de Documento Nacional de Identidad (DNI) en 7. 

También hoy cobran los beneficiarios de las asignaciones universales por Hijo (AUH) y las familiares con documentos finalizados en 4, y por Embarazao (AUE) en 2, indicó la Administración Nacional de la Seguridad Social (Anses),

Del mismo modo lo harán los jubilados y pensionados con DNI terminados en 4 y cuyos haberes no superen la suma de $20.374.

El cronograma del IFE continuará mañana nuevamente con los documentos cuyo último número es 7.

En tanto, las personas con DNI terminado en 8 cobrarán este miércoles y este jueves; y los finalizados en 9, este viernes y el lunes próximo, 21 de septiembre.

Para consultas sobre fechas y lugar de cobro se puede ingresar a www.anses.gob.ar/ife, con el número de DNI y clave de la seguridad social del beneficiario.