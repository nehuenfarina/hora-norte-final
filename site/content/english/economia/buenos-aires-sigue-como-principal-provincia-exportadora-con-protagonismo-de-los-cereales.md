+++
author = ""
date = 2020-10-20T03:00:00Z
description = "Buenos Aires tuvo más de un tercio de las exportaciones, seguida por Santa Fe, Córdoba, Chubut y Santa Cruz, informó el Indec. A causa de la pandemia las ventas de productos primarios crecieron un 14,1% interanual en el primer semestre."
image = "/images/599b1481632eb_1004x564.jpg"
image_webp = "/images/599b1481632eb_1004x564-1.jpg"
title = "Buenos Aires sigue como principal provincia exportadora, con protagonismo de los cereales"

+++
###### La soja y sus derivados representaron el 28,1% del total de las ventas externas a nivel país.

La provincia de Buenos Aires encabezó el acumulado exportador a nivel país en el primer semestre, tras alcanzar **el 35,3% del comercio total por un monto de US$9.650 millones**, apalancado por las ventas externas de trigo, maíz y cebada que representaron el 20,5% del total provincial.  
  
Santa Fe con el 20,1% del total nacional, Córdoba con el 15,9%, Chubut con el 3,6% y Santa Cruz con el 3,1% acompañaron a Buenos Aires, de acuerdo al informe del Instituto Nacional de Estadística y Censos (Indec) sobre el origen provincial de las exportaciones en los primeros seis meses de este año.  
  
En la provincia líder, **el rubro de cereales fue seguido por el de residuos y desperdicios de la industria alimenticia** (esencialmente subproductos oleaginosos de soja), que tuvieron un incremento interanual de 65,0% y alcanzaron los US$1.151 millones durante el semestre.  
  
A continuación, figuró el rubro material de transporte terrestre **(fundamentalmente integrado por automóviles),** que con US$1.096 millones significó el 11,4% de las exportaciones de la provincia; seguido por carnes y sus preparados que creció 5,7% interanual y alcanzó exportaciones por US$725 millones.  
  
**En tanto, la provincia de Santa Fe** comercializó por un total de US$5.486 millones (20,1% del total país), y tuvo como rubro destacado a residuos y desperdicios de la industria alimenticia (principalmente harinas y pellets de soja), con 39,4% del total provincial.  
Le siguieron en importancia las exportaciones de grasas y aceites (de soja, en su mayoría) y cereales (maíz y trigo), que representaron 19,2% y 12,9% del total exportado, respectivamente.

![Santa Fe aportó el 20,1% de las exportaciones nacionales.](https://www.telam.com.ar/advf/imagenes/2020/05/5eb9b0ae7c9fb_1004x565.jpg "Santa Fe aportó el 20,1% de las exportaciones nacionales.")_Santa Fe aportó el 20,1% de las exportaciones nacionales._

  
**En tercer lugar se posicionó Córdoba** con el 15,9% del total país y un monto de US$4.339 millones (un crecimiento del 1,6% respecto a igual período de 2019).  
Los principales productos exportados fueron cereales –principalmente maíz y trigo–, con el 32,2% del agregado provincial; residuos y desperdicios de la industria alimenticia –mayoritariamente harinas y pellets de soja–, con el 20,4%; y semillas y frutos oleaginosos (principalmente porotos de soja y maní) el 17,3%.  
  
**En cuarto y quinto lugar aparecieron las provincias patagónicas de Chubut y Santa Cruz,** con el 3,6% y el 3,1% de participación en el total país, respectivamente.  
  
**Chubut** exportó por US$ 975 millones, una baja de 30,4% respecto del año anterior explicada por la reducción de ventas en los rubros líderes como petróleo crudo que disminuyó 37,8% y cuyos despachos representan el 38,4% de las exportaciones, y mariscos sin elaborar y pescados que redujo sus ventas un 17,6% y participó con el 27,9% del total.  
  
Por el lado de **Santa Cruz** las ventas externas totalizaron US$846 millones, lo que significó una caída de 27,6% respecto de igual período de 2019.  
El principal rubro de exportación fue piedras y metales preciosos, que resultó el 66,2% del total provincial y registró una baja de 26,8%; seguido de pescados y mariscos sin elaborar, por un 11,4% de las ventas al exterior y sufrió una caída de 13,4%; y petróleo crudo, que alcanzó una participación de 11,2% a pesar de la disminución de 37,8% registrada en el período.  
  
**En cuanto a la región cuyana, la provincia más destacada fue Mendoza** que vendió por US$661 millones y tuvo una participación del 2,4% en el total nacional, explicada principalmente por la comercialización de vinos de uva y hortalizas y legumbres sin elaborar.  
  
**En el Noroeste argentino (NOA) sobresalió Santiago del Estero**, con ventas exteriores por US$559 millones (2% del total país), principalmente de maíz y trigo y semillas y frutas oleaginosas.  
  
**Las ventas del Noreste argentino (NEA)** fueron comandadas por Chaco con US$246 millones -un 0,9% del total nacional-, generados fundamentalmente por el comercio de cereales y frutas y semillas oleaginosas.  
  
Así, en el acumulado por regiones, la Pampeana explicó el 75,5% del total exportador a nivel país; la Patagonia el 8,7%, el NOA el 5,9%; Cuyo el 5,8%; y el NEA el 2,1%.  
  
No obstante el desagregado por provincias y rubros, **el complejo sojero explicó el 28,1% del total de las ventas externas a nivel país** en el período analizado, seguido por los complejos maicero, petrolero-petroquímico y triguero con el 12,2%, 7% y 6,8%, respectivamente.  
  
A causa de la alteración que la pandemia del coronavirus produjo en el comercio internacional, **las ventas de productos primarios crecieron un 14,1% interanual en el primer semestre** para explicar el 34,1% del total país (US$9.333 millones).  
  
En contrapartida, las manufacturas de origen agropecuario (MOA) se contrajeron un 8,4% interanual, y representaron el 38,2% del total país (US$10.449 millones); y las manufacturas de origen industrial (MOI) se desplomaron un 34,6%, ubicándose en el 21,5% del total nacional por un monto de US$5.882 millones.  
También **los combustibles y la energía sufrieron una pronunciada contracción** del orden del 24,2%, registrando ventas por US$1.673 millones.  
  
Así, las exportaciones totales registraron un declive del 11% en el primer semestre del año, presentando un resultado de $27.336 millones.