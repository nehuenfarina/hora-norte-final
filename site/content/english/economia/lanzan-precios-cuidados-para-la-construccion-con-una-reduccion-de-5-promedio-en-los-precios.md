+++
author = ""
date = 2020-08-18T03:00:00Z
description = "El anuncio fue realizado por el presidente Alberto Fernández en un acto desarrollado este mediodía en la planta de producción de la empresa de griferías Hidromet, en el partido bonaerense de San Fernando."
image = "/images/5f5fc03da2889_1004x565-1.jpg"
image_webp = "/images/5f5fc03da2889_1004x565.jpg"
title = "Lanzan Precios Cuidados para la Construcción con una reducción de 5% promedio en los precios"

+++
###### _Alberto Fernández estuvo acompañado por Axel Kicillof y otros funcionarios nacionales y provinciales._

El anuncio fue realizado por el presidente Alberto Fernández durante un acto desarrollado este mediodía en la planta de producción de la empresa de griferías Hidromet, en el partido bonaerense de San Fernando.

El Gobierno nacional lanzó hoy el programa de Precios Cuidados para la Construcción, que comprende a 93 productos que estarán disponibles en 500 bocas de expendio con una rebaja promedio del 5% respecto a los vigentes en el mercado.

{{< youtube 9zvDyOrGgU0 >}}

_Alberto Fernández presentó los Precios Cuidados para la Construcción_

El programa diagramado por la Secretaría de Comercio Interior -cuyo listado se puede consultar en el [sitio ](https://www.argentina.gob.ar/precios-cuidados/construccion)incluye 93 productos, de 17 rubros para la obra gruesa y de terminación.

**Los productos incluidos son aberturas, arena, cal, cemento, cerámicos y porcelanatos, chapa, grifería, hierro, ladrillo, pinturas, placa de cemento, placa de yeso, productos de aislación e impermeabilización, de electricidad, de iluminación, sanitarios y yeso.**

Precios Cuidados de la Construcción estará disponible en más de 500 bocas de expendio en todo el país de las cadenas Easy, Sodimac, Blaisten y distintas locales de las pinturerías Prestigio, Rex, Sagitario, Pintecord y Universo, entre otras.

Entre los productos ofrecidos se encuentran marcas líderes como: Ferrum, Roca, FV, Hidromet, Peirano, Saint gobain, Durlock, Acindar, Loma Negra, Siderar, Osram, Philips, Cerámica San Lorenzo, Cañuelas, entre otras.

El acuerdo establece una reducción de 5% a los precios promedio vigentes en el mercado en la actualidad. A modo de ejemplo, la bolsa de cemento pasará a costar $560 contra $619 de la actualidad.

En tanto, la bolsa de cal bajará de $384 a $358, el ladrillo hueco de $36 a $33, la chapa de $2.517 a $2.345 la unidad, inodoro y depósito de $19.499 a $16.702, grifería de cocina de $8.299 a $7.515, pintura por 20 litros de $4.490 a $3.890, entre otros productos y precios.

"Los precios de estos artículos son valores de referencia y no de oferta, con el objetivo de que las y los consumidores tengan disponibles una orientación a la hora de realizar sus compras", señaló la Secretaría de Comercio Interior en un comunicado.

En la presentación del programa, el ministro de Desarrollo Productivo, Matías Kulfas, dijo que "la construcción va a ser un sector fundamental en la etapa de reactivación. Ya estamos viendo datos, como el Índice Construya que mide la venta de productos para la construcción y que en agosto se incrementó un 2,3% y ya acumula 3 meses consecutivos de crecimiento”.

Al respecto, el presidente de la Cámara de Empresas de Servicios Inmobiliarios (Camesi), Alejandro Ginevra, dijo a Télam que "hay expectativa en todo el sector, de los desarrolladores, constructores y también del sector inmobiliario, tanto con el anuncio de precios para la construcción como el de la CNV del viernes, que busca alternativas para que los ahorristas inviertan en el desarrollo inmobiliario".

"Ambas son medidas necesarias de acompañamiento, que apoyamos y que desde nuestro sector venimos pidiéndole al Gobierno en este contexto para la recuperación de la economía real y la reactivación del sector, que todavía sigue siendo poca", agregó.

Por su parte, el asesor inmobiliario Daniel Zampone, señaló a esta agencia que "lo que nos va a sacar de la crisis es la construcción, va a reactivar la economía y va a dar un alivio a los que les falta techo. Estos anuncios los vemos positivos, son pequeñas señales que vamos viendo en el camino".

A su turno, el desarrollador Horacio Ludigliani afirmó que "es muy positiva una iniciativa de este tipo, nos da oxígeno. Uno puede observar en la Ciudad muchas demoliciones en marcha porque aquel que está 'líquido' aprovecha este momento en el cual el metro cuadrado está muy barato en dólares".

"En materia de construcción, el 70% del costo de obra es en pesos. Es una oportunidad para desarrolladores e inversores", sostuvo en diálogo con Télam y proyectó que "pasada esta pandemia, vendrán buenos años como pasó en 2003 y 2004 después de la crisis de 2001. Es nuestra responsabilidad infundir mucha confianza en el consumidor final y mirar sólo hacia adelante".

En tanto, el corredor Oscar Puebla dijo a Télam que esta medida "es muy alentadora y va de la mano con la creciente demanda de terrenos ya que se nota la necesidad de la gente de cambiar el estilo de vida; el plan del Gobierno es muy oportuno y va seguir tentando a inversores".

En las últimas semanas, la ministra de Desarrollo Territorial y Hábitat, María Eugenia Bielsa, adelantó que existían negociaciones para poner en marcha el relanzamiento del programa para poder dar un marco de referencia al sector, de cara a las iniciativas relacionadas con el impulso de la construcción y el Procrear.

El objetivo "es poder garantizar relaciones responsables con los precios; no sería bueno que este esfuerzo que hacen todos los argentinos de poner muchos recursos en la construcción y que eso se traslade a precios", dijo y remarcó que "es muy importante que apelemos a la responsabilidad colectiva".

El programa de Precios Cuidados tendrá una vigencia anual, con renovaciones trimestrales de precios, a partir de un acuerdo alcanzado con proveedoras y comercializadoras de materiales para la construcción.

"Los Precios Cuidados de la Construcción estarán disponibles en los próximos días en todos los comercios y acorde avance el programa se promoverá una mayor participación de proveedores y comercializadoras y se buscará incrementar la variedad en la oferta de productos", informó el Ministerio de Desarrollo Productivo.