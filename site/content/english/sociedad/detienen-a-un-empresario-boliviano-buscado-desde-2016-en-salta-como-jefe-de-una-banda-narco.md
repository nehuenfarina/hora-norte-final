+++
author = ""
date = 2021-01-28T03:00:00Z
description = ""
image = "/images/wilson-maldonado-balderrama.jpg"
image_webp = "/images/wilson-maldonado-balderrama.jpg"
title = "Detienen a un empresario boliviano buscado desde 2016 en Salta como jefe de una banda narco"

+++

**_Se trata del empresario Wilson Maldonado Balderrama (65), sobre quien pesaba una orden de captura con cédula roja de Interpol desde el 2019 como sospechoso de ser el dueño de la cocaína secuestrada en el operativo "Febrero Blanco"._**

Un empresario boliviano, dueño de hipódromos en su país y dedicado a la cría de caballos, fue detenido en Santa Cruz de la Sierra acusado de ser el jefe de una banda dedicada al narcotráfico en Argentina, al que se le adjudica un cargamento de 267 kilos de cocaína que había sido secuestrado en 2015 en Salvador Mazza y por el cual ya hay dos exconcejales detenidos.

Fuentes judiciales aseguraron a Télam que se trata del empresario Wilson Maldonado Balderrama (65), sobre quien pesaba una orden de captura con cédula roja de Interpol desde el 2019 como sospechoso de ser el dueño de la cocaína secuestrada en el operativo "Febrero Blanco", cuya particularidad era que los panes de droga hallados tenían un sello en el frente con una "W".

Fuentes judiciales aseguraron a Télam que la detención del empresario se concretó ayer en Santa Cruz de la Sierra, Bolivia, y que el hombre se encuentra alojado en la cárcel de Palmasola, acusado del delito de "transporte y contrabando de estupefacientes".

#### La detención

Según la comunicación de Interpol, a la que tuvo acceso Télam, el hombre fue apresado cuando se movilizaba a bordo de una camioneta Toyota Tundra por el kilómetro 14 de la avenida Doble Vía a la Guarda, de Santa Cruz de la Sierra, y no opuso resistencia.

La detención la concretaron efectivos de Interpol Bolivia y del Grupo de Reacción Inmediata (GRI) del Departamento de Santa Cruz, y que el hombre apresado quedó a disposición de la jueza en lo penal de esa ciudad boliviana, Esther Ocampo, a la espera de que se inicien los trámites de extradición a la Argentina.

El empresario está acusado por la Justicia Federal salteña de tener vinculación con la operativo denominado "Febrero Blanco" de 2015, cuando efectivos de Gendarmería Nacional interceptaron un camión que circulaba por la ruta nacional 16, en Salvador Mazza, y hallaron acondicionados 267,549 kilos de cocaína en panes que llevaban como logo una "W".

Por orden del juez federal 1 de Salta, Julio Bavio, fueron detenidos dos hombres que se movilizaban a bordo del camión y, pocos meses después, fue apresado un concejal, llamado Alejandro Maurín.

Tanto los dos camioneros como el concejal fueron llevados a juicio en 2018 y todos ellos fueron condenados: el edil recibió una pena de 14 años de prisión, que aún está purgando.

En tanto, el juez también ordenó la captura de otro concejal, llamado Mauricio Gerónimo, quien permaneció prófugo durante años hasta que fue detenido en abril del 2019 precisamente en Santa Cruz de la Sierra, Bolivia, durante un operativo conjunto de la policía local y Gendarmería Nacional Argentina (GNA).

Gerónimo, que es técnico en Economía y solo estuvo dos meses como integrante del Concejo Deliberante de Salvador Mazza, fue apresado mientras preparaba su fiesta de cumpleaños en Santa Cruz de la Sierra, donde residía con una identidad falsa, dijeron los investigadores en aquel momento.

Para la Justicia argentina, Maurín y Gerónimo eran los líderes de la facción local de la banda narco que comandaba, desde Bolivia, Balderrama.

Una de las claves para llegar a Balderrama era la "W" con las que era individualizada la cocaína; la otra fue la agenda del concejal Maurín, que tenía a "W" Balderrama entre sus contactos, dijeron las fuentes.

Precisamente, el juez Bavio ya había ordenado la captura internacional de este empresario boliviano el 30 de marzo del 2016 por ser el proveedor de la cocaína de "Febrero Blanco" y fue detenido en su país ese mismo año, pero como tenía causas pendientes allí con la justicia local, se rechazó su extradición.

En Bolivia, el hombre estuvo acusado por "lavado de activos" y, tras unos meses, fue excarcelado.

Fuentes judiciales aseguraron a Télam que en 2019 el juez Bavio insistió con el pedido de captura de Balderrama e Interpol lo puso entre sus prioridades, con cédula roja, por lo que efectivos de esa fuerza internacional en Bolivia finalmente lo hallaron ayer en Santa Cruz de la Sierra.

La sospecha de los investigadores es que la cocaína que proveía supuestamente Balderrama, era luego trasladada a distintos centros urbanos del país, en especial a Rosario, Córdoba o Buenos Aires, donde se secuestraron panes con el sello característicos de la "W".