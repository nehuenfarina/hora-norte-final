+++
author = "InfoUrbana"
date = 2020-09-14T21:20:12Z
description = "Según un estudio de la Facultad de Psicología de la UBA, los mayores niveles de prejuicio en la Argentina recayeron sobre los inmigrantes latinoamericanos."
image = "/images/discriminado-1.jpg"
image_webp = "/images/discriminado-1.jpg"
title = "Alrededor del 40% de los argentinos se sintió discriminado en el último año"

+++
_Según un estudio de la Facultad de Psicología de la UBA, los mayores niveles de prejuicio en la Argentina recayeron sobre los inmigrantes latinoamericanos._

El estudio **“Prejuicio y discriminación en Argentina (2020)”** elaborado en base a **1.534 respuestas recibidas**, señaló que el **52,6% de las personas encuestadas se sintió discriminada por cuestiones políticas.**  
  
A la discriminación por cuestiones ideológicas, **le sigue la edad (25,9%) y luego una condición física o mental (24,1%).**  
  
Del 40% de las personas que se sintió discriminada, **las mujeres y las personas mayores representaron los índices más altos.**  
  
En cuanto a los prejuicios, **los mayores niveles se presentaron hacia los inmigrantes latinoamericanos, seguido por las personas con discapacidad mental y los menores hacia personas homosexuales y aquellas que contrajeron coronavirus.**  
  
La posición frente al aborto muestra que el **55% está de acuerdo con legalizar y despenalizar el aborto y el 45% opinó que debe dejarse tal como está.**  
  
Según la edad, se manifestó a favor de la legalización y despenalización el 61,7% de la franja etaria de 18 a 33, el 51,5% de 34 a 54 y un 51,9% de 55 a 75 años.  
  
Según indica, un tercer grupo se conforma por la autopercepción de discriminación por género (15%) y religión (14,3%) y, finalmente, “un último grupo que indica haberse sentido discriminado por su orientación sexual (9,9%), por su etnia (6,5%) y por Covid-19 (4,8%)”.  
  
Teniendo en cuenta los prejuicios según la edad, los jóvenes de entre 18 y 33 años presentaron mayores niveles hacia la obesidad, y los participantes con edades de entre 55 y 75 años mostraron “mayores niveles de prejuicio sexista, tanto en su forma hostil como benevolente”.  
  
El prejuicio hacia la homosexualidad se presentó de forma escalonada, de menores a mayores niveles según la edad de los participantes.  
  
Con respecto a los ámbitos donde las y los participantes experimentaron algún tipo de discriminación, el porcentaje más alto declarado es el espacio público (67,2%), en segundo lugar se ubica el laboral (36%), y el tercer lugar el familiar (22,9%), mientras que el más bajo es el sector educativo (20,3%).  
  
Los niveles de acuerdo con el cupo laboral trans en organismos del Estado marcó que hay una mayor inclinación hacia ello, con un 42,3%, sumando “algo de acuerdo” (13,2%) con “totalmente de acuerdo” (29,1%), en contraste con el desacuerdo que llega al 34,2%, sumando “algo en desacuerdo” (8,2%) con “totalmente en desacuerdo” (26%).  
  
En el ítem “Autopercepción de discriminación según el género”, la investigación señaló que si bien “quienes se autoperciben con género femenino declararon haberse sentido discriminadas en mayor medida, en comparación con quienes se autoperciben con el género masculino, no se hallaron diferencias estadísticamente significativas entre las variables.  
  
Asimismo, un 33,2% indicó nunca haber vivido ni conocer a nadie que haya atravesado una situación de acoso sexual, un 66,8% de las y los encuestados dio cuenta de que sí, aunque en diferentes frecuencias, un 37,6% indicó “alguna vez” y un 10,8% señaló la respuesta “muchas veces”.  
  
“Al desglosar los resultados según el género, se observan diferencias significativas particularmente en la respuesta ‘nunca’, siendo 43,3% el porcentaje de masculinos, mientras que un 24,3% el porcentaje para el género femenino”, se indicó.  
  
En tanto, "se puede observar que el 85% de las mujeres encuestadas en la Ciudad de Buenos Aires sufrió́ alguna situación de acoso sexual callejero, sumado a que el 62% de los varones participantes consideró que a las mujeres les gustan los 'piropos' callejeros".