+++
author = ""
date = 2021-01-20T03:00:00Z
description = ""
image = "/images/rachel-levine.jpg"
image_webp = "/images/rachel-levine.jpg"
title = "EEUU: Rachel Levine, una mujer trans, a cargo de la subsecretaria de Salud"

+++

**_El presidente electo sostuvo que la actual secretaria de Salud de Pensilvania, “aportará el liderazgo constante y la experiencia esencial que necesitamos para que la gente supere esta pandemia”. Se convertiría en la primera funcionaria federal abiertamente transgénero en ser confirmada por el Senado._**

El presidente electo de los Estados Unidos, Joe Biden, anunció este martes la nominación de Rachel Levine para ocupar el cargo de subsecretaria de Salud. De esta manera, Levine se convertiría en la primera funcionaria federal abiertamente transgénero en ser confirmada por el Senado de Estados Unidos.

En Estados Unidos los altos cargos de gobierno requieren la venia de la Cámara Alta, que pasará a ser controlada por los demócratas después de que Biden y la vicepresidenta electa, Kamala Harris, tomen posesión el miércoles. El oficialismo retomó el control del Senado a principios de enero, cuando sus dos candidatos se impusieron en sendas elecciones especiales en el estado de Georgia, llevando de esa manera el número de bancas “azules” a 50. Considerando que en caso de empate será Harris quien desempate las votaciones, los demócratas efectivamente controlarán la cámara.

“Levine aportará el liderazgo constante y la experiencia esencial que necesitamos para que la gente supere esta pandemia, sin importar su código postal, raza, religión, orientación sexual, identidad de género o discapacidad, y para satisfacer las necesidades de salud pública de nuestro país en este momento crítico”, señaló Biden en una declaración al anunciar la nominación.

“Ella es una opción histórica y profundamente calificada para ayudar a liderar los esfuerzos de salud de nuestra administración”, apuntó.

Levine deberá jugar un rol preponderante en la respuesta a numerosos desafíos sanitarios que la administración de Biden heredará al llegar a la Casa Blanca. Entre ellas se destaca predeciblemente la rampante pandemia del coronavirus, que al martes se cobró la vida de más de 400.000 personas en el país en prácticamente un año.

El mandatario electo ha anticipado las claves de su plan para contrarrestar el impacto de la pandemia: prevé vacunar a 100 millones de personas durante sus primeros 100 días en el cargo y buscará miles de millones de dólares del Congreso para financiar centros de inmunización masiva. También pedirá la habilitación de 130.000 millones de dólares para financiar testeos regulares que permitan la reapertura de las escuelas, y hará un llamado a que la población use mascarilla durante al menos los primeros 100 días de su mandato.

Levine es actualmente secretaria de Salud del estado de Pensilvania y profesora de pediatría y psiquiatría en la Escuela de Medicina de la Universidad Penn State.

Graduada de Harvard y de la Escuela de Medicina de Tulane, Levine es presidente de la Asociación de Funcionarios de Salud Estatales y Territoriales. Ha escrito en el pasado sobre la crisis de los opiáceos, la marihuana medicinal, la medicina para adolescentes, los desórdenes alimenticios y la medicina LGBTQ, entre otros temas.

Pediatra de formación, el último año fue la cara pública de la respuesta al covid-19 de Pennsylvania. Anteriormente, dirigió una clínica de salud para adolescentes en la Universidad Estatal de Pensilvania antes de ser nombrada médico general por el gobernador del estado, Tom Wolf, y luego fue confirmada unánimemente como secretaria de salud en 2017.

Biden nominó al político de California Xavier Becerra como secretario de Salud. Si el Senado lo confirma, Becerra se convertirá en el primer hispano en encabezar el HHS.

Por su parte, Jeff Zients será el coordinador del equipo conformado por el flamante presidente norteamericano para hacer frente al coronavirus.