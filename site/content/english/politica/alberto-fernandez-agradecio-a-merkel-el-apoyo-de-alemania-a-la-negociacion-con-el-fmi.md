+++
author = ""
date = 2021-01-26T03:00:00Z
description = ""
image = "/images/alberto-y-angela.jpg"
image_webp = "/images/alberto-y-angela.jpg"
title = "Alberto Fernández agradeció a Merkel el apoyo de Alemania a la negociación con el FMI"

+++

**_“Necesitamos que nos sigan acompañando en el acuerdo que eventualmente logremos”, le transmitió el mandatario argentino a la canciller alemana._**

El presidente Alberto Fernández agradeció este lunes a la canciller alemana Angela Merkel el apoyo que el país europeo viene brindando a la negociación que lleva adelante Argentina con el Fondo Monetario Internacional (FMI) y le pidió que sigan acompañando "en el acuerdo que eventualmente" se alcance entre las partes.

“Necesitamos que nos sigan acompañando en el acuerdo que eventualmente logremos”, le transmitió Fernández a Merkel durante una videoconferencia que se desarrolló esta tarde por espacio de 40 minutos.

Según se informó oficialmente, el diálogo sirvió para repasar las negociaciones que lleva adelante Argentina con el FMI y también las que deberá establecerse con el Club de París para “reestructurar de manera sostenible la deuda” externa.

Por su parte, la canciller sostuvo que su equipo de asesores económicos viene siguiendo de cerca la cuestión del FMI y el Club de París, donde espera que se alcance el acuerdo que la Argentina necesita.

"Siempre los hemos apoyado y así seguiremos haciéndolo", expresó la líder alemana.

La reunión se produjo a días de cumplirse un año de la primera que mantuvieron ambos líderes, el 3 de febrero de 2020, durante la gira europea que realizó el presidente argentino para conseguir adhesiones a su plan de renegociación de la deuda externa con acreedores privados.

En aquella oportunidad, la canciller alemana había ponderado el diálogo que había establecido la Casa Rosada con los acreedores privados y con el Fondo Monetario Internacional.

La opinión de Alemania -integrante del G7 y líder económico en la Unión Europea- puede convertirse en central en lo que respecta a las negociaciones que por estos días lleva adelante el ministro Martín Guzmán con el organismo de crédito.

En otro tramo de la conversación, Fernández -desde su rol de presidente pro tempore del Mercosur- llamó a profundizar el vínculo del bloque regional con Alemania y Europa para así avanzar con diversos acuerdos: “Tenemos vínculos muy profundos que tenemos que seguir desarrollando”.

“Los líderes abordaron también temas de interés común en la agenda regional, tras el cambio de gobierno en los Estados Unidos, interesándose particularmente Merkel en la situación de América latina y el diálogo entre la Argentina y los distintos países de la región”, se destacó desde la Presidencia.

Ambos intercambiaron opiniones respecto a cuestiones ambientales y del cambio climático, y destacaron el avance de los acuerdos en el sector automotriz, donde la empresa alemana Volkswagen es líder en Argentina.

En diciembre de 2020, Fernández participó de la inauguración de la nueva planta de pintura de la empresa automotriz, en el Centro Industrial Pacheco, en el marco de la inversión por 650 millones de dólares que la compañía lleva adelante para la producción del modelo Taos.

Finalmente, Fernández le informó a su par alemana que sigue de cerca lo que sucede con la pandemia de coronavirus en Europa, le transmitió el avance del plan de vacunación en el país y los acuerdos logrados con los distintos laboratorios en pos de la adquisición de las distintas vacunas contra el virus.

El contacto que mantuvo este lunes con Merkel se suma a otro que el Presidente tuvo la semana pasada con la directora gerente del Fondo Monetario Internacional (FMI), Kristalina Georgieva.

En ese oportunidad, ambos coincidieron en que se continuará trabajando en un programa apoyado por el organismo multilateral y diseñado y conducido por la Argentina".