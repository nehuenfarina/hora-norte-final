+++
author = ""
date = 2021-02-01T03:00:00Z
description = ""
image = "/images/leopoldo-lopez.jpg"
image_webp = "/images/leopoldo-lopez.jpg"
title = "El opositor Leopoldo López acusado de planear un atentado explosivo contra el Parlamento"

+++

**_El presidente de la Asamblea Nacional (AN, parlamento) de Venezuela, el oficialista Jorge Rodríguez, afirmó que el dirigente opositor Leopoldo López planeó un supuesto ataque explosivo a la sede del Legislativo y pidió explicaciones por el caso a autoridades de España, donde reside el acusado._**

Jorge Rodríguez dijo en conferencia de prensa que “Desde España, el señor Leopoldo López planificó un ataque con bombas a la sede de la AN de Venezuela; vamos a mostrar todas las pruebas y fue capturada la persona que iba a colocar los aparatos explosivos en medio de una sesión de la AN la semana pasada”.

Rodríguez, uno de los dirigentes de mayor confianza del presidente Nicolás Maduro, vinculó ese supuesto plan con el tiempo que López pasó refugiado en la sede de la embajada de España en Caracas, entre mayo de 2019 y octubre de 2020, y con el entonces embajador español, Jesús Silva.

“Silva fue el mayordomo de Leopoldo López mientras se encontraba en la residencia del embajador de España en Venezuela; todos los delitos, todos los recursos que se dispusieron para el entrenamientos de desertores y terroristas, todo se planificó con la complicidad de Jesús Silva”, sostuvo Rodríguez.

“¿Qué tiene que decir el gobierno del reino de España sobre esto?”, se preguntó el legislador, y añadió: “Algo deben decir el parlamento español y el presidente (del gobierno, Pedro) Sánchez ante esta serie de actos en la embajada de España en Venezuela”, según la agencia de noticias Europa Press.

Asimismo, Rodríguez informó que la semana pasada fue capturado un capitán desertor del Ejército, al que identificó como Juan Gutiérrez Aranguren y al que se refirió como “la persona que iba a colocar los aparatos explosivos” en la AN.

Además, lo acusó de haber formado parte de la Operación Gedeón, una incursión marítima fallida realizada en mayo de 2020 y que el gobierno atribuyó entonces a “mercenarios”.

Rodríguez exhibió un video en el que el supuesto Gutiérrez Aranguren confiesa que recibió entrenamiento en Colombia para realizar incursiones armadas en Venezuela.