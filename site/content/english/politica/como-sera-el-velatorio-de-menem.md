+++
author = ""
date = 2021-02-14T03:00:00Z
description = ""
image = "/images/_112912221_gettyimages-106282527.jpg"
image_webp = "/images/_112912221_gettyimages-106282527.jpg"
title = "¿Cómo será el velatorio de Menem?"

+++

**_La despedida prevista incluirá un momento íntimo con su familiares para luego permitir el ingreso de dirigentes, amigos y allegados hasta el lunes, cuando se retiren sus restos para su inhumación._**

El expresidente Carlos Menem será velado este domingo desde las 20 en el Salón Azul del Senado de la Nación, adonde podrá ingresar el público para dar su adiós al exmandatario, informaron fuentes oficiales.

El cuerpo del senador riojano, quien falleció a los 90 años, será llevado al Palacio Legislativo cuando terminen las tareas previas en una casa velatoria, tal lo dispuesto por su hija Zulema Menem, su exesposa Zulema Yoma y otros familiares, explicaron a Télam fuentes de la Presidencia de la Cámara alta.

Las fuentes oficiales precisaron que "la capilla ardiente comenzará a las 20 horas y el público deberá ingresar por la explanada del Palacio, en la avenida Rivadavia y Entre Ríos".

La familia decidió además no permitir acceso a la prensa, y solo habrá transmisión institucional por Senado TV y su canal de YouTube y se contará con el servicio de fotografía de la Cámara alta.