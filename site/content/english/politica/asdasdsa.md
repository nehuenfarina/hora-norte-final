+++
author = ""
date = 2020-09-14T20:25:00Z
description = "Alberto Fernandez \"el gran dilema es hoy entre los que creen en un Estado asociado al que produce y a los trabajadores\" y los que creen que \"es un país para pocos\""
image = "/images/5f5f9ea3527a6-09-2020-565_1004x565.jpg"
image_webp = "/images/5f5f9ea3527a6-09-2020-565_1004x565.jpg"
title = "Alberto Fernández: \"No queremos multiplicar la estafa que vivimos los últimos cuatro años\""

+++
_Alberto Fernandez "el gran dilema es hoy entre los que creen en un Estado asociado al que produce y a los trabajadores" y los que creen que "es un país para pocos"_

El presidente Alberto Fernández afirmó hoy que su Gobierno "no quiere multiplicar la estafa que vivimos los últimos cuatro años" y señaló que existe "un gran dilema entre quienes creen en un Estado asociado al que produce y los que llevan su dinero a paraísos fiscales mientras sumergen en la pobreza a la Argentina".

"El verdadero dilema está entre los que creemos que el Estado, asociado a los que producen y trabajan, puede hacer un país que integre a todos y los que creen que Argentina es un país para pocos", dijo Fernández al presentar el programa Precios Cuidados para la construcción en una fábrica en el partido bonaerense de San Fernando.

**El mandatario criticó a quienes "están convencidos de que es mejor negocio tener el dinero afuera invertido en paraísos fiscales para no pagar impuestos y se enriquecen, y potencian el valor de las fortunas que seguramente heredaron o construyeron evadiendo impuestos"**.

"Pero hay empresarios que sacan la plata de su bolsillo y arriesgan: esa es la Argentina que queremos potenciar y no otra", ya que "un país que sumerge en la pobreza al 40% quitando el trabajo y promoviendo la especulación, es una Argentina que le sirve a muy pocos y queremos que nos sirva y nos incluya a todos", sostuvo.

> "Un país que sumerge en la pobreza al 40% quitando el trabajo y promoviendo la especulación, es una Argentina que le sirve a muy pocos"
>
> **ALBERTO FERNÁNDEZ**

Fernández estuvo acompañado por el gobernador bonaerense, Axel Kicillof; el ministro de Desarrollo Productivo, Matías Kulfas; el intendente local, Juan Andreotti; la titular de Aysa, Malena Galmarini, autoridades nacionales, provinciales y municipales, y directivos y trabajadores de Hidromet.

**"Cuando en una sociedad alguien gana mucho y otros pierden mucho, eso no es una sociedad, sino que se parece más a una estafa y nosotros no queremos multiplicar la estafa que hemos vivido los últimos cuatro años"**, añadió.

Luego de repasar la historia reciente de Hidromet, que en los últimos cuatro años tuvo que reducir a la mitad su personal y su producción, el mandatario apuntó que ese modelo de país "no es una buena república", ya que "la familia queda desamparada porque se perdió un trabajo y los productos no se producen". En ese contexto, el mandatario no dudó en asociar "la victoria" a "la producción y el trabajo".

Por su parte, **Kicillof dijo que "es mentira que el Estado estuvo ausente durante las épocas neoliberales: estuvo presente para patear en contra, porque una tasa de interés de 80% requiere una decisión del Estado de sostenerla"**.

**"La decisión de defender la timba, la fuga de capitales y la generación de un modelo extractivista y primarizador impide que puedan prosperar y sobrevivir estas cosas maravillosas que existen en la provincia por todos lados"**, destacó.

Además, el mandatario bonaerense aseguró que "no es que no sirva la industria nacional, sino que se necesitan políticas adecuadas para prosperar, como las que está tomando el Gobierno nacional".

> "La industria necesita tener un mercado que le compre, un sistema financiero que le preste y un Estado que la ayude"
>
> **AXEL KICILLOF**

"La industria necesita tener un mercado que le compre, un sistema financiero que le preste y un Estado que la ayude", pero en el gobierno anterior se dio "la tormenta perfecta: tasas de interés altas, consumo bajo e importaciones abiertas", consideró.

El gobernador concluyó que "la prioridad del Gobierno es la producción y el trabajo nacional, y eso es consumo y bienestar para los argentinos".