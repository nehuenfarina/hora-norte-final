+++
author = ""
date = 2021-04-06T03:00:00Z
description = ""
image = "/images/200226110932-coronavirus-doctors-wuhan-china-0224.jpg"
image_webp = "/images/200226110932-coronavirus-doctors-wuhan-china-0224.jpg"
ttile = "MURIERON 272 PERSONAS Y 13667 FUERON DIAGNOSTICADAS CON CORONAVIRUS EN EL PAÍS"

+++
**_Otras 272 personas murieron y 13.667 fueron reportadas con coronavirus en las últimas 24 horas en la Argentina, con lo que ascienden a 56.471 los fallecidos registrados oficialmente a nivel nacional y a 2.407.159 los contagiados desde el inicio de la pandemia, informó este lunes el Ministerio de Salud._**

La cartera sanitaria indicó que son 3.652 los internados en unidades de terapia intensiva, con un porcentaje de ocupación de camas de adultos de 56,1% en el país y del 62% en la Área Metropolitana Buenos Aires.

Un 63,44% (8.671 personas) de los infectados de este lunes (13.667) corresponden a la Ciudad y a la provincia de Buenos Aires.

De los 2.407.159 contagiados, el 89,46% (2.153.509) recibió el alta y 197.179 son casos confirmados activos.

#### Las vacunas

De acuerdo al Monitor Público de Vacunación actualizado a las 18, el total de inoculados asciende a 4.354.044, de los cuales 3.663.455 recibieron una dosis y 690.589 las dos, mientras que las vacunas distribuidas a las jurisdicciones llegan a 5.893.445.

El reporte consignó que murieron 138 hombres y 128 mujeres, mientras que 4 persona de la provincia de Buenos Aires, 1 de la Ciudad de Buenos Aires, y 1 en Córdoba fueron registradas sin dato de sexo.

#### Los fallecidos

El parte precisó que murieron 78 hombres en la provincia de Buenos Aires; 22 en la Ciudad de Buenos Aires; 4 en Chaco; 3 en Corrientes; 3 en Córdoba; 1 en Jujuy; 3 en Mendoza; 1 en Misiones; 3 en Río Negro; 6 en Salta; 4 en San Luis; 2 en Santa Cruz; 4 en Santa Fe; y 4 en Santiago del Estero.

También fallecieron 81 mujeres en Buenos Aires; 18 en Ciudad de Buenos Aires; 8 en Chaco; 3 en Córdoba; 3 en Mendoza; 2 en Salta; 4 en San Luis; 1 en Santa Cruz; 7 en Santa Fe y 1 en Santiago del Estero.

Este lunes se registraron en la provincia de Buenos Aires 6.243 casos; en la Ciudad de Buenos Aires, 2.428; en Catamarca, 110; en Chaco, 90; en Chubut, 154; en Corrientes, 60; en Córdoba, 1048; en Entre Ríos, 107; en Formosa, 30; en Jujuy, 46; en La Pampa, 138; en La Rioja, 43; en Mendoza, 597; en Misiones, 87; en Neuquén, 192; en Río Negro, 147; en Salta, 82; en San Juan, 144; en San Luis, 270; en Santa Cruz, 187; en Santa Fe, 843; en Santiago del Estero, 122; Tierra del Fuego, 59 y en Tucumán 440.

#### Los testeos

El Ministerio indicó además que se realizaron en las últimas 24 horas 46.037 testeos y desde el inicio del brote ascienden a 9.116.035 pruebas diagnósticas para esta enfermedad.

El total de acumulados por distrito indica que la provincia de Buenos Aires suma 1.016.493 casos; la Ciudad de Buenos Aires, 270.868; Catamarca, 10.869; Chaco, 39.110; Chubut, 50.489; Corrientes, 30.932; Córdoba 184.912; Entre Ríos, 50.707; Formosa, 2.349; Jujuy, 22.693; La Pampa, 21.740; La Rioja, 11.404; Mendoza, 74.954; Misiones, 12.396; Neuquén, 65.690; Río Negro, 55.878; Salta, 29.824; San Juan, 17.621; San Luis, 23.888; Santa Cruz, 39.975; Santa Fe, 234.641; Santiago del Estero, 25.758; Tierra del Fuego, 24.364 y Tucumán, 89.604.

Se incluyen 17 casos existentes en las Islas Malvinas según información de prensa (debido a la ocupación ilegal del Reino Unido, Gran Bretaña e Irlanda del Norte no es posible contar con información propia sobre el impacto de la Covid-19 en esa parte del territorio argentino).

#### PANORAMA NACIONAL

En este contexto, el jefe de Gabinete Santiago Cafiero encabezó el lunes por la tarde en Casa Rosada una reunión junto a sus pares de Provincia y de la Ciudad, Carlos Bianco y Felipe Miguel, respectivamente; la ministra de Salud, Carla Vizzotti; el titular de la cartera sanitaria de CABA, Fernán Quirós; y el viceministro de Salud bonaerense, Nicolás Kreplak; en reemplazo del ministro provincial, Daniel Gollan.

Los funcionarios coincidieron, tras otra reunión de los ministros de salud de todas las provincias en el Consejo Federal de Salud (Cofesa), que la llegada de la segunda ola "es un hecho".

Si bien este lunes no hubo anuncios tras el encuentro, las fuentes remarcaron que habrá que "tomar medidas" en los próximos días, en principio el viernes próximo, cuando venza una nueva prórroga del Decreto de Necesidad y Urgencia sobre el Distanciamiento Social Preventivo y Obligatorio (Dispo).

Por su parte, infectólogos y miembros del comité de expertos que asesora al Gobierno nacional advirtieron que "estamos claramente en la segunda ola" que podrían impulsar "restricciones más importantes" para evitar la circulación del virus.

La infectóloga Laura Barcán dijo a Télam que "estamos claramente en la segunda ola, es lo que se dio en todo el mundo” y advirtió que "el aumento de casos tan brusco no lo vimos nunca en la primera ola, esto es exponencial".

En el mismo sentido se manifestó la infectóloga Leda Guzzi, quien aseguró que hay un explosivo aumento del número de casos" y precisó que “en la última semana aumentaron los casos entre un 40 y 50% dependiendo de las jurisdicciones, entre ellas AMBA y centros urbanos como Córdoba, Mendoza o Corrientes".

El médico clínico integrante del comité que asesora al presidente Alberto Fernández, Luis Cámera, alertó que "esta ola viene adelantada sobre las proyecciones que se tenían dos semanas atrás; viene adelantada y a mucha velocidad”.

#### PANORAMA MUNDIAL

En Brasil, el país con la situación más severa de Sudamérica, Río de Janeiro volvió a abrir las escuelas, pero el comercio no esencial, bares, restaurantes, cines, museos y teatros siguen cerrados hasta el viernes con horarios restringidos.

Las estimaciones prevén que en Brasil se llegará a 600.000 muertes en el país en julio.

Paraguay, en el peor momento de la pandemia en el país, con hospitales públicos y privados colapsados, amplió por ocho días la franja horaria de circulación de 5 a 24 y los locales gastronómicos podrán recibir clientes previa inscripción y cumpliendo las medidas sanitarias.

Uruguay terminó su Semana de Turismo (como llaman a la Semana Santa) con la noticia de un brote en un geriátrico en la ciudad de Fray Bentos que dejó 15 muertos.

En Chile, el presidente Sebastián Piñera admitió que el sistema de salud está "al límite de sus capacidades", después de registrar más de 7.000 casos positivos de coronavirus en los últimos tres días y superar la semana pasada el millón de contagios desde el inicio de la pandemia.

En ese marco, Bolivia mantiene hasta el jueves el cierre de su frontera con Brasil.

Europa, en cambio, diversificó sus conductas pese al alarmante incremento de casos, ya que mientras el Reino Unido y Serbia, entre otros, aflojaron en algo sus restricciones, otros países optaron por mantenerlas y hasta endurecerlas.

El primer ministro Boris Johnson confirmó la continuidad del desconfinamiento en el Reino Unido a partir del próximo lunes cuando la segunda fase permita la reapertura de peluquerías, gimnasios y pubs, entre otros servicios.

Por su parte, el ministro de Salud de Francia, Olivier Veran, pidió cuidados extremos y advirtió que la cantidad de pacientes con coronavirus en las unidades de terapia intensiva del país podría alcanzar los niveles observados durante la primera ola de hace un año.

Los comercios minoristas en la mayor parte de Grecia pudieron reabrir este lunes a pesar del aumento continuo de las infecciones por coronavirus.

También Portugal inició la segunda fase de su plan de desconfinamiento progresivo al reabrir la hostelería y los museos y permitir la vuelta a clase de alumnos del segundo y tercer ciclo, hasta los 15 años.