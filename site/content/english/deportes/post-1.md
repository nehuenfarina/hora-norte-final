---
title: Testeos PCR y ejercicios tácticos en River
date: 2020-09-14T20:40:34.000+00:00
image: "/images/river-cuarentena.jpg"
author: ''
description: Los resultados de los hisopados para detectar si hay Covid 19 se conocerán
  esta noche y recién entonces el DT Marcelo Gallardo sabrá que futbolistas están
  autorizados a viajar a Brasil para el cotejo ante San Pablo.
categories:
- Green Tree
tags:
- green
- tree
image_webp: "/images/river-cuarentena.jpg"

---
_Los resultados de los hisopados para detectar si hay Covid 19 se conocerán esta noche y recién entonces el DT Marcelo Gallardo sabrá que futbolistas están autorizados a viajar a Brasil para el cotejo ante San Pablo._

El plantel de River Plate fue hisopado esta mañana en el predio de Ezeiza antes del ensayo táctico del posible equipo para jugar este jueves con San Pablo, en Brasil, por la tercera fecha del Grupo D de la Copa Libertadores.

A la espera de los resultados que determinarán qué jugadores queden en condiciones de viajar el miércoles, el DT Marcelo Gallardo trabajó con una formación que incluyó como novedad al lateral izquierdo Fabricio Angileri en lugar de Milton Casco, quien dio positivo de Covid-19 la semana pasada y está descartado para el juego con los brasileños.

El posible once que tiene el "Muñeco" en mente incluye a Franco Armani; Gonzalo Montiel, Lucas Martínez Quarta, Javier Pinola y Angileri; Julián Álvarez, Ignacio Fernández, Enzo Pérez y Nicolás de la Cruz; Matías Suárez y Rafael Borré.

Los resultados de los hisopados se conocerán esta noche y recién entonces Gallardo tendrá certeza sobre los futbolistas autorizados a viajar a Brasil, dado que Casco podría haber contagiado a algún compañero durante la práctica de fútbol del jueves pasado.

Como indicio alentador, ningún jugador del plantel mostró síntomas compatibles al coronavirus después de conocerse el positivo del marcador izquierdo, aunque alguno de ellos podría estar transitando la enfermedad de manera asintomática.

El aislamiento de Casco se produjo el viernes tras los síntomas que manifestó un integrante de su grupo familiar que, al dar positivo, obligó al defensor a testearse.

El protocolo sanitario de la Conmebol impide que un jugador, miembro del cuerpo técnico o auxiliar participe de un partido en caso de dar positivo de Covid-19 en los exámenes obligatorios dentro de los cinco días previos.

Luego de los hisopados realizados esta mañana, el plantel continuó con su rutina de entrenamientos con pelota en espacios reducidos y diferentes actividades físicas mientras esperan la chance de realizar una práctica de fútbol antes de jugar el jueves.

El plantel de River volverá al trabajo mañana en el predio de Ezeiza con trabajos técnicos con pelota a fin de poder llegar al partido del jueves ante San Pablo lo mejor posible tras seis meses sin competencia oficial.

El último partido del equipo de Gallardo fue ante Binacional de local cuando ganó 8-0 el 11 de marzo pasado en la segunda fecha de la Copa Libertadores ya que luego no jugó ante Atlético Tucumán en la primera fecha de la Copa de la Superliga que luego se canceló.  
San Pablo, en cambio, volvió a los entrenamientos hace dos meses y lleva jugados 13 partidos oficiales entre el torneo Paulista y el Brasileirao, en el que marcha segundo tras 10 juegos.

El viaje a San Pablo está programado para el miércoles a las 16 horas desde Ezeiza en un vuelo chárter. La delegación se alojará en el hotel Morumbi y luego del partido del jueves regresará en el mismo vuelo.

Para ese encuentro, River sufrirá también la ausencia del delantero Lucas Pratto por una distensión muscular en el isquiotibial izquierdo.

Tanto Casco como el "Oso" tampoco podrán jugar el siguiente partido ante Binacional en Lima el martes 22 de septiembre y llegarán con los tiempos justos para la revancha ante San Pablo del 30 de septiembre en el estadio de Independiente.

En el grupo de River, los equipos comparten la primera ubicación de la tabla producto de un triunfo y una derrota cada uno, pero el "Millonario" ocupa el primer lugar por diferencia de gol.

Mañana se reanudará la competencia en el grupo con la visita de Liga de Quito a Binacional en Lima ya que el equipo peruano no fue autorizado a jugar en la altura de Juliaca.