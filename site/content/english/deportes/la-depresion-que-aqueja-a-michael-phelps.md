+++
author = ""
date = 2021-01-25T03:00:00Z
description = ""
image = "/images/1470700591_227958_1470701368_noticia_normal.jpg"
image_webp = "/images/1470700591_227958_1470701368_noticia_normal.jpg"
title = "La depresión que aqueja a Michael Phelps"

+++

**_El ex nadador estadounidense había explicado en una carta pública en 2020 que debió acostumbrarse a transitar la enfermedad y que sus hijos son clave en su lucha diaria._**

En enero de 2018, Michael Phels le confesó al mundo que a pesar de sus logros sufría una profunda depresión que lo había dejado al borde del suicidio. En aquel entonces, explicó en varias entrevistas que esa etapa oscura de su vida había sido superada, pero luego, en mayo de 2020, reconoció que eso era mentira.

Según una carta que escribió para el sitio de ESPN, el nadador atravesó un mal momento emocional a causa de la cuarentena por la pandemia del coronavirus. Como muchos establecimientos en donde podría entrenar estaban cerrados para evitar el contagio, él había perdido lugares en donde explotar todas las emociones que antes utilizaba para mejorar su rendimiento.

Esta semana quien se refirió al asunto fue Nicole, esposa del ex deportista. En diálogo con el sitio Today, la mujer dio algunos detalles sobre el mal momento que atraviesa su marido y aclaró que la depresión es algo constante en su vida aunque tiene bajones en los que esta queda más expuesta. Es ahí donde ella y sus hijos hacen todo lo posible para ayudarlo y demostrarle que no está solo en esta lucha constante.

“Los chicos quieren estar cerca de Michael cuando tiene un día difícil. Quieren intentar hacerlo feliz, especialmente Boomer porque es el mayor. Así que decimos, ‘Hey Booms, papá lo está pasando mal y solo necesita tomarse un momento para estar solo’. Queremos que Boomer entienda que no se trata de él, se trata de Michael”, contó Nicole quien reveló que su retoño es uno de los pilares de la familia: “Me está ayudando con todo. Es un apoyo para mí, pero más que nada, la terapia me proporciona las herramientas para poder ayudar a Michael correctamente”, explicó.

En los Juegos Olímpicos de Beijing 2008, Michael Phelps hizo historia al colgarse ocho medallas de oro, algo que ningún deportista había logrado hasta entonces. El ex nadador dejó atrás el récord de Mark Spitz en Múnich 1972 y llevó a su país a la gloria al sumar preseas doradas en 200 metros libre, 100 metros mariposa, 200 metros mariposa, 200 metros midley, 400 metros midley, 4×100 libre, 4×200 libre, 4×100 midley, estableciendo un nuevo récord mundial en casi todas las competencias, a excepción de 100 metros mariposa, en el que también rompió la marca olímpica.

Tras una vida de éxito, se despidió de la actividad en Río 2016, Juegos en los que acumuló cinco oros y una plata, pero su éxito personal no ha exceptuado al estadounidense de esta enfermedad que lo aqueja desde hace tiempo.

“Solía pensar que podía arreglarlo, ser su terapeuta, lo que él necesita. Pero lo que he aprendido es que no puedes responsabilizarte por cómo se siente, no importa cuánto le quieras”, declaró Nicole, quien admitió tener miedo de perder a su esposo: “Después de que Vanessa perdió a Kobe (Bryant), todo lo que pude hacer fue mirar a Michael y decir, ‘¿Podemos ayudarte? Porque si te pierdo, no sé qué voy a hacer’”.

En su carta pública de 2020, el ex nadador de 35 años se mostró agradecido por no tener problemas económicos como muchas familias en el mundo, porque sino habría otra preocupación en su cerebro. Pero a pesar de que está rodeado de su familia y que ha superado el peor momento de sus conflictos, fue sincero: “Aquí está la realidad: nunca me curaré. Esto nunca desaparecerá. Es algo en lo que he tenido que aceptarlo, aprender a lidiar con eso y convertirlo en una prioridad en mi vida. Y sí, es mucho más fácil decirlo que hacerlo”.

“Hay momentos en los que me siento absolutamente inútil, donde me apago por completo pero tengo esta ira burbujeante que está por las nubes. Si soy honesto, más de una vez grité en voz alta: ‘¡Ojalá no fuera yo!’. A veces hay una sensación abrumadora de que no puedo soportarlo más. Ya no quiero ser yo”, explicó sobre qué siente.

En aquel escrito Phelps había reconocido que sus hijos son de gran ayuda porque sus abrazos y sonrisas le permiten olvidarse de sus demonios, como él mismo los definió, y disfrutar de un rato sin ansiedad ni preocupaciones. Sin embargo, tanto él como su familia han tenido que acostumbrarse a vivir con episodios constantes de negatividad.