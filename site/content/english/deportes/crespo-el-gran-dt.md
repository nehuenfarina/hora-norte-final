+++
author = ""
date = 2021-01-26T03:00:00Z
description = ""
image = "/images/crespo.jpg"
image_webp = "/images/crespo.jpg"
title = "Crespo, el gran DT"

+++

**_Antes de darle concepción al flamante ganador de la Copa Sudamericana, el excentrodelantero del seleccionado argentino hizo una experiencia en el Módena de la segunda división de Italia y luego hizo su debut en Argentina con un Banfield que ya mostraba rasgos de su propuesta de juego pero que no pudo consolidarse por la falta de futbolistas de vocación goleadora._**

Entre varias aristas dignas de elogio, la espléndida coronación de Defensa y Justicia pone en entredicho la extendida creencia de que sin una materia prima de primer nivel es imposible competir en las altas cumbres con posibilidades de éxito y coloca a Hernán Crespo en el centro de un escenario que se ha ganado con creces.

Nada más lejos que deducir que Defensa y Justicia ha jugado bien por el sólo de hecho de haber ganado la Copa Sudamericana y que el logro en sí representa un absoluto que clausura todo discernimiento conceptual.

Por lo contrario, la secuencia es inversa y lo suficientemente visible como para abrirse en camino por fuera de un mero juego de palabras: más bien Defensa y Justicia se hizo fuerte jugando bien y en todo caso la conquista tiene rango de fruto maduro.

En alguna medida Crespo tomó las cosas donde la dejaron media docena de entrenadores que, a grandes rasgos, comparten el mismo ideario futbolístico o por lo menos propiciaron modos similares: Jorge Almirón, Diego Cocca, Ariel Holan, Sebastián Beccacece, Juan Pablo Vojvoda y Mariano Soso.

(Quien lo deseara podría agregar los breves interregnos de Darío Franco, Turu Flores, Nelson Vivas y Pablo De Muner).

Desandado el camino de la impronta de Defensa y Justicia en esa línea de continuidad, encontraremos sorprendentes puntos en común: por ejemplo planteles austeros (jugadores cedidos por clubes grandes, experimentados del Ascenso, juveniles de procedencia variopinta) y equipos insospechados de ataduras, con agresividad bien entendida y una férrea vocación de ejercer el protagonismo en cada partido o, en el peor de los casos, de compartirlo.

Tan marcados y pasmosos esos indicadores comunes que transmiten la sensación de que Walter Busse y Walter Acevedo eran hermanos mellizos, y Enzo Fernández el hermano menor de los dos; y algo similar sucede con Alexander Barboza, Lisandro Martínez y Héctor Martínez.

Por misterioso que parezca, y lo es, o no tanto, hace seis años o más aún que en el Halcón de Florencio Varela hay como mínimo un defensor central que amén de conocer el oficio de marcar, sabe proponerse como fundador de cada avance y hay también un mediocampista central entendedor, pasador y criterioso.

No tan misteriosa es la mano de Crespo, el ex "Valdanito" campeón con seis camisetas (River, Parma, Lazio, Milan, Chelsea, Inter), que a menos de una década de saltar a la dirección técnica propició un equipo con todo en su lugar.

Antes de darle concepción al flamante ganador de la Copa Sudamericana, el excentrodelantero del seleccionado argentino hizo una experiencia en el Módena de la segunda división de Italia y luego hizo su debut en Argentina con un Banfield que ya mostraba rasgos de su propuesta de juego pero que no pudo consolidarse por la falta de futbolistas de vocación goleadora.

Este Defensa y Justicia es un equipo de esos que inspiran una exclamación tan añeja como el deporte de la pelota número 5: "¡Qué bien juega!".

¿Y qué será jugar bien? ¿Una suma de acrobacias?

Pues no: jugar bien es desarrollar el potencial disponible, establecer una buena relación con la pelota, con los espacios y con la exigencia que demanda cada partido sin alejarse demasiado de la orilla de los que se dan en llamar "identidad".

Un equipo ni temeroso ni temerario, que ataca con muchos y defiende con muchos y se siente a gusto en el ejercicio de una presión ni ansiosa ni morosa que le permite fijar las condiciones y el ritmo del juego.

Según se ve, más temprano que tarde Crespo ha encarnado el apotegma primordial de Marcelo Bielsa, uno de los técnicos más significativos que tuvo cuando futbolista: postulaba el rosarino que los entrenadores no tienen otra opción que imponer lo que piensan.